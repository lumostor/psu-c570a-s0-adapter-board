#!/usr/bin/env python3

from KicadModTree import *

footprint_name = "Connector-FCI-51866-044LF"

pin_base_y = -(2.54+1.27)
pin_pitch = 2.54
# init kicad footprint
kicad_mod = Footprint(footprint_name)
kicad_mod.setDescription("Power to the Board PWRBLADE RCPT R/A 30P")
kicad_mod.setTags("PWRBLADE Power Board RCPT connecto ")

# set general values
kicad_mod.append(Text(type='reference', text='REF**', at=[-30, -9], layer='F.SilkS'))
kicad_mod.append(Text(type='value', text=footprint_name, at=[20, -9], layer='F.Fab'))

# create silscreen
# 64.26 x 15.49
kicad_mod.append(RectLine(start=[-32.13, -15.49+pin_pitch*1.5+3.81],
                          end=[32.13,pin_pitch*1.5+3.81],
                          layer='F.SilkS'))

# create courtyard
#kicad_mod.append(RectLine(start=[-2.25, -2.25], end=[5.25, 2.25], layer='F.CrtYd'))
kicad_mod.append(Line(start=[-34.13, pin_pitch*1.5+3.81],
                      end=[34.13, pin_pitch*1.5+3.81],
                      layer='F.CrtYd'))
kicad_mod.append(Text(type='user', text="PCB EDGE", at=[0,pin_pitch*1.5+3.81+1],
                      layer='F.CrtYd'))
# create pads
# kicad_mod.append(Pad(number=1, type=Pad.TYPE_THT, shape=Pad.SHAPE_RECT,
#                      at=[0, 0], size=[2, 2], drill=1.2, layers=Pad.LAYERS_THT))
# kicad_mod.append(Pad(number=2, type=Pad.TYPE_THT, shape=Pad.SHAPE_CIRCLE,
#                      at=[3, 0], size=[2, 2], drill=1.2, layers=Pad.LAYERS_THT))
pwr_base_x = 20.32 - pin_pitch/2  #-(2.54*2+1.27)  # pin 1 upper left
pwr_base_y = -(2.54+1.27)
for pwr in range(1,7):
    for i in range(8):
        #print('{},{},{},{}'.format(i, pwr_base_y+(i/2)*pin_pitch, i/2, i//2))
        kicad_mod.append(Pad(number='P'+str(pwr), type=Pad.TYPE_THT, shape=Pad.SHAPE_CIRCLE,
                             at=[pwr_base_x-(pwr-1)*pin_pitch*2+(i%2)*pin_pitch,
                                 pwr_base_y+(i//2)*pin_pitch],
                             size=[2, 2], drill=1.02, layers=Pad.LAYERS_THT))

sig_base_x = -9.53
sig_base_y = 1.27+2.54
for sig_row in range(6):
    for i,sig_col in enumerate(['A', 'B', 'C', 'D']):
        kicad_mod.append(Pad(number=sig_col+str(sig_row+1), type=Pad.TYPE_THT,
                             shape=Pad.SHAPE_CIRCLE,
                             at=[sig_base_x-(sig_row)*pin_pitch,
                                 sig_base_y-i*pin_pitch],
                             size=[2, 2], drill=1.02, layers=Pad.LAYERS_THT))

#mounting holes NPTH
kicad_mod.append(Pad(number='~', type=Pad.TYPE_NPTH, shape=Pad.SHAPE_CIRCLE,
                     at=[-55.88/2, pin_pitch/2], size=[2, 2], drill=4.01,
                     layers=Pad.LAYERS_NPTH))
kicad_mod.append(Pad(number='~', type=Pad.TYPE_NPTH, shape=Pad.SHAPE_CIRCLE,
                     at=[55.88/2, pin_pitch/2], size=[2, 2], drill=4.01,
                     layers=Pad.LAYERS_NPTH))
                     
# add model
# kicad_mod.append(Model(filename="example.3dshapes/example_footprint.wrl",
#                        at=[0, 0, 0], scale=[1, 1, 1], rotate=[0, 0, 0]))

# output kicad model
file_handler = KicadFileHandler(kicad_mod)
file_handler.writeFile(footprint_name+'.kicad_mod')
